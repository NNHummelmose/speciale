function [X_tilde, principal,eigen, sigma] = Mp_denoise(data,nr_trial ,channel_nr,input)
%% Tjekker om det givne data er struct 

A = isstruct(data);

if A == 1
%% Indl�ser EEG data, og definerer variable
[nb,nD] = size(data.trial{nr_trial}) ;%antal elektroder, antal m�linger

% time = data.time{nr_trial};
X = data.trial{nr_trial};

elseif A == 0
nb = size(data,1) ;%antal elektroder
nD = size(data,2) ; %antal m�linger

X = data;
end
%% Udregner mean og centrer datapunkterne
mean_vec = mean(X,2); % tager r�kkevis
X_mean = (X- mean_vec); 
%% SVD - metode til at sammenligne med
%svd sorterer v�rdierne efter st�rste - s� beh�ver jeg ikke senere.
[U, SVD, V] =  svdecon(X_mean/sqrt(nD)); % Fordelingen passer
%dividerer med sqrt(nD) for at f� normaringen X = sqrt(nD)*U*sigma*V^T

eigen = diag(SVD).^2 ;
%% Fitter til MP fordeling, bruger SVD 
principal = 0; g_p = 0; % definerer da de skal bruges uden for for-l�kken

number_of_principal = true;
p = 0 ; 
 
while number_of_principal 
    g_p =(nb-p)/nD;
    a = sum(eigen(p+1:end));
    sig_2 = (eigen(p+1)-eigen(end))/(4*sqrt(g_p));
    c = (nb-p)*sig_2;
  number_of_principal = a < c;
    principal = p;
    p = p+1;
end

% principal = 1;
%Omdefinerer sigma, for at tage h�jde for noget bias
sigma = sqrt(sum(eigen(principal+1:end))/(nb-principal));

principal;
%%  Plotter forholdet mellem lambda og sigma som fkt af p
p =0; gg_p =0;
if input == true 
    k =0;
    for p = 0:length(eigen)-1
        k = k+1;
   gg_p =(nb-p)/nD;
   mean_la(k) = sum(eigen(p+1:end));
   sigma_2(k) = (nb-p)*(eigen(p+1)-eigen(end))/(4*sqrt(gg_p));
  
    end
    figure
    hold on
    box on
    ax = gca;
    ax.LineWidth = 1.5;
    grid on
    xlabel('\langle \lambda \rangle','FontSize',13)
    ylabel('\sigma^2','FontSize',13)
    plot(mean_la,sigma_2,'k*')
    plot(mean_la(1:principal),sigma_2(1:principal),'r*')
    line(xlim,xlim)
    hold off
end
%% Plotter egenv�rdier ud fra MP fordeling
gamma = g_p;

lambda_min = ((sigma^2*(1-sqrt(g_p))^2));
lambda_max = (sigma^2*(1+sqrt(g_p))^2);

lambda = linspace(lambda_min,lambda_max,5000);

%Mp fordeling
ft=@(lambda,a,b,gamma,sigma) (1./(2*pi*lambda*gamma*sigma^(2))).*sqrt((a-lambda).*(lambda-b));
MP = ft(lambda,lambda_max,lambda_min,gamma,sigma);

if channel_nr ~= 0
figure
hold on
box on
ax = gca;
ax.LineWidth = 1.3
title('Distribution of eigenvalues','FontSize',13)
plot(lambda,MP,'g','LineWidth',2)
xlabel('Eigenvalue(\lambda)','FontSize',13)
ylabel('Probability density function','FontSize',13)
hi = histogram(eigen(principal+1:end),'Normalization','pdf');
hi.NumBins = 30;
legend('MP distribution','Eigenvalues')
hold off
end

%% Laver vores nye vektorer, kun med v�rdier der ikke er st�j
reduced_svd = eigen;
reduced_svd(principal+1:end) = 0;


%tager sqrt for at komme tilbage til single value istedet for egenv�rdi
Lambda_tilde = [sqrt(diag(reduced_svd)) zeros(nb,nD-nb)]; %laver matrix st�rrelsen rigtig

%Laver rekonstrueret data
X_tilde = (sqrt(nD)*U(:,1:principal)*SVD(1:principal,1:principal)*V(:,1:principal)')+mean_vec;




%% Ser om de matcher
if channel_nr ~= 0
figure
hold on
box on
ax = gca;
ax.LineWidth = 1.3;
% title('Denoised vs noised signal for a given position','FontSize',13)
xlabel('Time','FontSize',13)
ylabel('Amplitude','FontSize',13)
plot(X(channel_nr,:))
plot(X_tilde(channel_nr,:))
legend('Original signal','Denoised signal')
hold off
end
end


function [U,S,V] = svdecon(X)
% Input:
% X : m x n matrix
%
% Output:
% X = U*S*V'
%
% Description:
% Does equivalent to svd(X,'econ') but faster
%
% Vipin Vijayan (2014)
%X = bsxfun(@minus,X,mean(X,2));
[m,n] = size(X);
if  m <= n
    C = X*X';
    [U,D] = eig(C);
    clear C;
    
    [d,ix] = sort(abs(diag(D)),'descend');
    U = U(:,ix);    
    
    if nargout > 2
        V = X'*U;
        s = sqrt(d);
        V = bsxfun(@(x,c)x./c, V, s');
        S = diag(s);
    end
else
    C = X'*X; 
    [V,D] = eig(C);
    clear C;
    
    [d,ix] = sort(abs(diag(D)),'descend');
    V = V(:,ix);    
    
    U = X*V; % convert evecs from X'*X to X*X'. the evals are the same.
    %s = sqrt(sum(U.^2,1))';
    s = sqrt(d);
    U = bsxfun(@(x,c)x./c, U, s');
    S = diag(s);
end
end